include std/filesys.e
include std/dll.e
include std/machine.e
include std/text.e
include std/search.e

sequence cmd =command_line()
global constant WAT=1
global constant GCC=2
global constant NON=0
global sequence weebp=pathname(cmd[1])&"\\"
global sequence eubin=pathname(cmd[1])&"\\"
ifdef EUI then
	weebp=match_replace("bin\\",weebp,"")
--	puts(1,weebp&"\n")
end ifdef
global integer compiler_count=0
global integer use_compiler=0

global procedure checkit(atom id=-1, sequence text="blahblah")
	if id <= 0 then
		puts(1,"Error : "& text&"\n")
	end if
end procedure

global function have_gcc()
atom result=0
    if file_exists(weebp&"GCC\\bin\\gcc.exe") then
        result = 1
        --puts(1,"Have GCC\n")
	else
		result = 0
    end if
return result    
end function

global function have_watcom()
atom result=0
    if file_exists(weebp&"WATCOM\\binnt\\wcc386.exe") then
        result = 1
        --puts(1,"Have WATCOM\n")
	else
		result = 0
    end if
return result
end function

compiler_count=have_gcc()+have_watcom()

if use_compiler=NON then
if compiler_count < 2 then
	if have_gcc() then
		use_compiler=GCC
	elsif have_watcom() then
		use_compiler=WAT
	else
		use_compiler=NON
	end if
else
	if use_compiler=NON then
		use_compiler=WAT -- default to Watcom
	end if
end if
end if



constant   
  ASSOCF_NONE                  = #00000, 
  ASSOCF_INIT_NOREMAPCLSID     = #00001, 
  ASSOCF_INIT_BYEXENAME        = #00002, 
  ASSOCF_OPEN_BYEXENAME        = #00002, 
  ASSOCF_INIT_DEFAULTTOSTAR    = #00004, 
  ASSOCF_INIT_DEFAULTTOFOLDER  = #00008, 
  ASSOCF_NOUSERSETTINGS        = #00010, 
  ASSOCF_NOTRUNCATE            = #00020, 
  ASSOCF_VERIFY                = #00040, 
  ASSOCF_REMAPRUNDLL           = #00080, 
  ASSOCF_NOFIXUPS              = #00100, 
  ASSOCF_IGNOREBASECLASS       = #00200, 
  ASSOCF_INIT_IGNOREUNKNOWN    = #00400, 
  ASSOCF_INIT_FIXED_PROGID     = #00800, 
  ASSOCF_IS_PROTOCOL           = #01000, 
  ASSOCF_INIT_FOR_FILE         = #02000 
  
enum   
  ASSOCSTR_COMMAND = 1, 
  ASSOCSTR_EXECUTABLE, 
  ASSOCSTR_FRIENDLYDOCNAME, 
  ASSOCSTR_FRIENDLYAPPNAME, 
  ASSOCSTR_NOOPEN, 
  ASSOCSTR_SHELLNEWVALUE, 
  ASSOCSTR_DDECOMMAND, 
  ASSOCSTR_DDEIFEXEC, 
  ASSOCSTR_DDEAPPLICATION, 
  ASSOCSTR_DDETOPIC, 
  ASSOCSTR_INFOTIP, 
  ASSOCSTR_QUICKTIP, 
  ASSOCSTR_TILEINFO, 
  ASSOCSTR_CONTENTTYPE, 
  ASSOCSTR_DEFAULTICON, 
  ASSOCSTR_SHELLEXTENSION, 
  ASSOCSTR_DROPTARGET, 
  ASSOCSTR_DELEGATEEXECUTE, 
  ASSOCSTR_SUPPORTED_URI_PROTOCOLS, 
  ASSOCSTR_MAX 
   
   
 
atom shwl=open_dll("shlwapi.dll") 
if shwl<0  then 
	puts(1,"shlwapi.dll not found!\n") 
	getc(0)
	abort(1) 
end if 

atom kernel32=open_dll("kernel32.dll") 
if shwl<0  then 
	puts(1,"kernel32.dll not found!\n") 
	getc(0)
	abort(1) 
end if 
 
 
atom getassoc=define_c_func(shwl,"AssocQueryStringA",{C_INT,C_INT,C_POINTER,C_POINTER,C_POINTER,C_POINTER},C_POINTER) 
if getassoc<0  then 
	puts(1,"AssocQueryStringA no found!\n") 
	getc(0) 
	abort(1) 
end if 

atom setconsoletitle=define_c_func(kernel32,"SetConsoleTitleA",{C_POINTER},C_INT) 
public function SetConsoleTitle(sequence title)
atom sztext=allocate_string(title)
atom result = c_func(setconsoletitle,{sztext})
free(sztext)
return result    
end function
 
global function GetAssoc(atom flag1,atom flag,sequence assoc,sequence extra) 
atom pzbuffer,pbuffer_size 
sequence result 
atom text=allocate_string(assoc) 
atom addon=allocate_string(extra) 
 
	pzbuffer=allocate(MAX_PATH) 
	pbuffer_size=allocate(4) 
	poke4(pbuffer_size,MAX_PATH) 
	poke4(pzbuffer,0) 
		c_func(getassoc,{flag1,flag,text,addon,pzbuffer,pbuffer_size}) 
	result = peek_string(pzbuffer) 
	free(pzbuffer) 
	free(pbuffer_size) 
	free(text) 
	free(addon) 
	return result 
 
end function 

global function GetAssocExe(object ext)
  return GetAssoc(ASSOCF_NONE,ASSOCSTR_EXECUTABLE,ext,"open")
end function
 
-- puts(1,GetAssocExe(".html")&"\n") 
-- getc(0)
constant MAX_PATH=260

 

atom myGetShortPathName=define_c_func(kernel32,"GetShortPathNameA",{C_POINTER,C_POINTER,C_UINT},C_INT)

global function ShortFileName (sequence filename)
atom inszpointer,outszpointer
object result
    outszpointer=allocate(MAX_PATH+1)
    inszpointer=allocate_string(filename)
    if not c_func(myGetShortPathName,{inszpointer,outszpointer,MAX_PATH+1}) then
		result=filename
	else
		result = peek_string(outszpointer)
	end if
    free(outszpointer)
    free(inszpointer)
    return result
end function

public procedure write_rc(sequence file)
atom fh
sequence sp=ShortFileName(pathname(file))
sequence progname=filebase(filename(file))
sequence icon_file=sp&"\\"&progname&".ico"
fh=open(file,"w")

if fh then
puts(fh,"#include <winver.h>\n") 
if file_exists(icon_file) then
    puts(fh,"ICON1 ICON "&progname&".ico\n")
else
	puts(fh,"//ICON1 ICON "&progname&".ico\n")
end if
  
puts(fh,"\n") 
puts(fh,"1 24 "&progname&".manifest.xml\n")  
puts(fh,"\n") 
puts(fh,"//-- version info (shown in file properties)\n")  
puts(fh,"VS_VERSION_INFO VERSIONINFO\n")  
puts(fh,"FILEVERSION 0, 8, 1, 5\n") 
puts(fh,"PRODUCTVERSION 0, 8, 1, 5\n")  
puts(fh,"FILEOS VOS__WINDOWS32\n")  
puts(fh,"FILETYPE VFT_APP\n")  
puts(fh,"// FILETYPE VFT_DLL\n")  
puts(fh,"\n")  
puts(fh,"BEGIN\n")  
puts(fh,"   BLOCK \"VarFileInfo\"\n")  
puts(fh,"   BEGIN\n") 

if equal(LANG,"EN") then 
	puts(fh,"    VALUE \"Translation\", 0x409, 1252  // Englisch (USA), Windows/Multilingual\n")  
else 
	puts(fh,"    VALUE \"Translation\", 0x407, 1252  // German,         Windows/Multilingual\n")  
end if

puts(fh,"   END\n")  
puts(fh,"\n")  
puts(fh,"   BLOCK \"StringFileInfo\"\n")  
puts(fh,"   BEGIN\n")

if equal(LANG,"EN") then   
	puts(fh,"    BLOCK \"040904E4\"                  // Englisch (USA), Windows/Multilingual\n")
else
	puts(fh,"    BLOCK \"040704E4\"                  // German,         Windows/Multilingual\n")
end if
  
puts(fh,"      BEGIN\n")  
puts(fh,"         VALUE \"FileVersion\",      \"0.815\\0\"\n")  
puts(fh,"         VALUE \"FileDescription\",  \""&progname&" PROGRAM\\0\"\n")  
puts(fh,"         VALUE \"LegalCopyright\",   \"(c) Author\\0\"\n")  
puts(fh,"\n")  
puts(fh,"         VALUE \"ProductVersion\",   \"0.815\\0\"\n")  
puts(fh,"         VALUE \"ProductName\",      \""&progname&" PROGRAM\\0\"\n")  
puts(fh,"         VALUE \"OriginalFilename\", \""&progname&".exe\\0\"\n")  
puts(fh,"         VALUE \"InternalName\",     \""&upper(progname)&"\\0\"\n")  
puts(fh,"         VALUE \"CompanyName\",      \"ACME Corp.\\0\"\n") 
puts(fh,"         VALUE \"Comments\",         \"Work in progress\\0\"\n")  
puts(fh,"      END\n")  
puts(fh,"   END\n")  
puts(fh,"END\n")  
close(fh)
else
	return
end if    
end procedure

public procedure write_manifest(sequence filename)
atom fh
fh=open(filename,"w")
if fh then
puts(fh,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n")
puts(fh,"<!-- These version numbers have nothing to do with\n")
puts(fh,"  EUPHORIA version numbers.  They must not be changed for\n")
puts(fh,"  version updates of EUPHORIA. -->\n")
puts(fh,"<assembly xmlns=\"urn:schemas-microsoft-com:asm.v1\" manifestVersion=\"1.0\">\n")
puts(fh,"<assemblyIdentity\n")
puts(fh,"    version=\"0.8.1.5\"\n")
puts(fh,"    processorArchitecture=\"x86\"\n")
puts(fh,"    name=\"YOUR PROGRAM\"\n")
puts(fh,"    type=\"win32\"\n")
puts(fh,"/>\n")
puts(fh,"<compatibility xmlns=\"urn:schemas-microsoft-com:compatibility.v1\">\n")
puts(fh,"    <application>\n")
puts(fh,"            <!-- Windows 10 -->\n")  
puts(fh,"            <supportedOS Id=\"{8e0f7a12-bfb3-4fe8-b9a5-48fd50a15a9a}\"/>\n") 
puts(fh,"            <!-- Windows 8.1 -->\n") 
puts(fh,"            <supportedOS Id=\"{1f676c76-80e1-4239-95bb-83d0f6d0da78}\"/>\n") 
puts(fh,"            <!-- Windows Vista -->\n") 
puts(fh,"            <supportedOS Id=\"{e2011457-1546-43c5-a5fe-008deee3d3f0}\"/>\n")  
puts(fh,"            <!-- Windows 7 -->\n") 
puts(fh,"            <supportedOS Id=\"{35138b9a-5d96-4fbd-8e2d-a2440225f93a}\"/>\n") 
puts(fh,"            <!-- Windows 8 -->\n") 
puts(fh,"            <supportedOS Id=\"{4a2f28e3-53b9-4441-ba9c-d69d4a4a6e38}\"/>\n") 
      
puts(fh,"    </application>\n")
puts(fh,"  </compatibility>\n")
puts(fh,"<dependency>\n")
puts(fh,"    <dependentAssembly>\n")
puts(fh,"        <assemblyIdentity\n")
puts(fh,"            type=\"win32\"\n")
puts(fh,"            name=\"Microsoft.Windows.Common-Controls\"\n")
puts(fh,"            version=\"6.0.0.0\"\n")
puts(fh,"            processorArchitecture=\"*\"\n")
puts(fh,"            publicKeyToken=\"6595b64144ccf1df\"\n")
puts(fh,"            language=\"*\"\n")
puts(fh,"        />\n")
puts(fh,"    </dependentAssembly>\n")
puts(fh,"</dependency>\n")
puts(fh,"</assembly>\n")
close(fh)
else
	return
end if
end procedure
